#include <LiquidCrystal_I2C.h>
#include <Arduino_FreeRTOS.h>

// Outputs config
const int SENSOR_INCENDIO = A2;
const int SENSOR_INUNDACAO = A1;
const int KEYBOARD = A0;

// Keyboard
int keyboard_state = 0;
int keyboard_pressed_index = -1;

//GLOBAL VARS
char pw[4] = {'1', '2', '3', '4'};
char pw_aux[4] = {'X', 'X', 'X', 'X'};
int inundation = 0;
int fire = 0;
int state_inundation_fire_alarm = 0;
//Alarm Mode
int alarm_mode = 0; // 0 = DISARMED | 1 = ARMED | 2 = ALERT | 3 = DANGER
int exit_alarm = 0;
//Keyboard aux
int count = 0;
int insertKey = 0;
//clocks
int clk_alert = 0;
int clk_presence = 0;

// DIGITAL PORTS
const int BOTAO_1 = 2;
const int BOTAO_2 = 3;
const int BUZZ = 5;
// 5 and 6 unused
const int LED_ALARM_VERDE = 7;
const int LED_ALARM_AMARELO = 8;
const int LED_ALARM_VERMELHO = 9;
const int LED_PRESENCA_1 = 10;
const int LED_PRESENCA_2 = 11;
const int LED_ALERT_INCENDIO = 12;
const int LED_ALERT_INUNDACAO = 13;

//TASKS
void Task_UpdateOutputs(void *param);
void Task_Led(void *param);
void Task_ReadSensors(void *param);
void Task_ReadKeyboard(void *param);
void Task_Clock(void *param);
void Task_Terminal(void *param);

//TASKS HANDLERS
TaskHandle_t Task_UpdateOutputs_Handle;
TaskHandle_t Task_Led_Handle;
TaskHandle_t Task_ReadSensors_Handle;
TaskHandle_t Task_ReadKeyboard_Handle;
TaskHandle_t Task_Clock_Handle;
TaskHandle_t Task_Terminal_Handle;

// keyboard values
int keyboard[] = {
    //1,   2,   3,   4,   5,   6,  7,  8,  9,  0,   A,   B,  C,  D,  *,  #
    252, 337, 505, 113, 129, 148, 73, 79, 85, 57, 993, 174, 94, 64, 54, 60};

LiquidCrystal_I2C lcd(0x27, 20, 4);

void setup()
{
  //pin mode
  pinMode(BUZZ, OUTPUT);
  pinMode(BOTAO_1, OUTPUT);
  pinMode(BOTAO_2, OUTPUT);
  pinMode(LED_ALARM_VERDE, OUTPUT);
  pinMode(LED_ALARM_AMARELO, OUTPUT);
  pinMode(LED_ALARM_VERMELHO, OUTPUT);
  pinMode(LED_ALERT_INCENDIO, OUTPUT);
  pinMode(LED_ALERT_INUNDACAO, OUTPUT);
  pinMode(LED_PRESENCA_1, OUTPUT);
  pinMode(LED_PRESENCA_2, OUTPUT);

  //Serial
  Serial.begin(9600);

  //Start lcd
  lcd.init();
  lcd.backlight();
  setDisplay(0);

  //TASKS CREATE
  xTaskCreate(Task_UpdateOutputs, "Task_UpdateOutputs", 60, (void *)0, tskIDLE_PRIORITY, &Task_UpdateOutputs_Handle);
  xTaskCreate(Task_Led, "Task_Led", 60, (void *)0, tskIDLE_PRIORITY, &Task_Led_Handle);
  xTaskCreate(Task_ReadSensors, "Task_ReadSensors", 50, (void *)0, tskIDLE_PRIORITY, &Task_ReadSensors_Handle);
  xTaskCreate(Task_ReadKeyboard, "Task_ReadKeyboard", 100, (void *)0, tskIDLE_PRIORITY, &Task_ReadKeyboard_Handle);
  xTaskCreate(Task_Clock, "Task_Clock", 80, (void *)0, tskIDLE_PRIORITY, &Task_Clock_Handle);
  //xTaskCreate(Task_Terminal, "Task_Terminal", 100, (void *)0, tskIDLE_PRIORITY, &Task_Terminal_Handle);
}

void loop()
{
}

//Update outputs
void Task_UpdateOutputs(void *param)
{
  (void)param;

  while (1)
  {
    if (alarm_mode == 0)
    {
      digitalWrite(LED_ALARM_VERDE, HIGH);
      digitalWrite(LED_ALARM_VERMELHO, LOW);
    }
    else if (alarm_mode == 1)
    {
      digitalWrite(LED_ALARM_VERDE, LOW);
      digitalWrite(LED_ALARM_VERMELHO, HIGH);
    }
    else
    {
      digitalWrite(LED_ALARM_VERDE, LOW);
      digitalWrite(LED_ALARM_VERMELHO, LOW);
    }
    vTaskDelay(200 / portTICK_PERIOD_MS);
  }
}

//
void Task_Led(void *param)
{
  (void)param;

  while (1)
  {
    if (alarm_mode == 3 || fire || inundation)
      digitalWrite(BUZZ, 1);
    else
      digitalWrite(BUZZ, 0);

    if (alarm_mode == 2)
    {
      digitalWrite(LED_ALARM_AMARELO, state_inundation_fire_alarm);
    }
    else
    {
      digitalWrite(LED_ALARM_AMARELO, 0);
    }

    if (alarm_mode == 3)
    {
      digitalWrite(LED_PRESENCA_1, 1);
      digitalWrite(LED_PRESENCA_2, 1);
      digitalWrite(LED_ALARM_AMARELO, 1);
    }
    else
    {
      if (clk_presence <= 10 || clk_presence >= 30)
      {
        digitalWrite(LED_PRESENCA_1, 0);
        digitalWrite(LED_PRESENCA_2, 0);
      }
      digitalWrite(LED_ALARM_AMARELO, 0);
    }

    inundation ? digitalWrite(LED_ALERT_INUNDACAO, !state_inundation_fire_alarm) : digitalWrite(LED_ALERT_INUNDACAO, 0);
    
    fire ? digitalWrite(LED_ALERT_INCENDIO, !state_inundation_fire_alarm) : digitalWrite(LED_ALERT_INCENDIO, 0);

    if(fire  || inundation || alarm_mode == 1)
      state_inundation_fire_alarm = !state_inundation_fire_alarm;
    else if(alarm_mode != 3)
      state_inundation_fire_alarm = 0;

    vTaskDelay(500 / portTICK_PERIOD_MS);
  }
}

// Ler sensores de movimento
void Task_ReadSensors(void *param)
{
  (void)param;

  while (1)
  {
    if (digitalRead(BOTAO_1) == HIGH || digitalRead(BOTAO_2) == HIGH)
      if (alarm_mode != 0 && alarm_mode != 3)
      {
        if(exit_alarm != 1)
          alarm_mode = 2;
      }

    fire = analogRead(SENSOR_INCENDIO) > 310 ? 1 : 0;

    inundation = analogRead(SENSOR_INUNDACAO) > 170 ? 1 : 0;

    vTaskDelay(110 / portTICK_PERIOD_MS);
  }
}

//Read Keyboard
void Task_ReadKeyboard(void *param)
{
  (void)param;

  while (1)
  {
    int val = analogRead(KEYBOARD); //Get current value

    if (val > 20 && keyboard_state == 0)
    {
      const int tolerancia = 2;
      for (int i = 0; i < 16; i++)
      {
        if ((val + tolerancia) >= keyboard[i] && (val - tolerancia) <= keyboard[i])
        {
          keyboard_pressed_index = i;
          keyboard_state = 1;
          break;
        }
      }
    }
    else if (val < 20 && keyboard_state == 1)
    {
      if (keyboard_state == 1)
      {
        if (alarm_mode != 2 && alarm_mode != 3 && !fire && !inundation)
        {
          digitalWrite(BUZZ, 1);
          vTaskDelay(100 / portTICK_PERIOD_MS);
          digitalWrite(BUZZ, 0);
        }

        char key = getKey(keyboard_pressed_index);

        if (key == 'A')
        {
          insertKey = 1;
          setDisplay(1);
        }
        else if (key == 'B')
        {
          insertKey = 0;
          count = 0;
          setDisplay(4);
        }
        else if (key == 'C')
        {
          insertKey = 0;
          count = 0;
          setDisplay(3);
        }
        else if (key == 'D')
        {
          insertKey = 0;
          count = 0;
          setDisplay(0);
        }
        else if (key == '*' && insertKey == 1)
        {
          count = 0;
          setDisplay(2);
        }
        else if ((key == '#' && insertKey == 1))
        {
          count = 0;
          insertKey = 0;
          if (checkPw())
            Enable_Disable_Alarm();
          else if (alarm_mode != 2)
          {
            digitalWrite(LED_ALARM_AMARELO, 1);
            digitalWrite(BUZZ, 1);
            vTaskDelay(300 / portTICK_PERIOD_MS);
            digitalWrite(LED_ALARM_AMARELO, 0);
            digitalWrite(BUZZ, 0);
            vTaskDelay(200 / portTICK_PERIOD_MS);
            digitalWrite(LED_ALARM_AMARELO, 1);
            digitalWrite(BUZZ, 1);
            vTaskDelay(300 / portTICK_PERIOD_MS);
            digitalWrite(LED_ALARM_AMARELO, 0);
            digitalWrite(BUZZ, 0);
          }
          setDisplay(0);
        }
        else if (insertKey == 1)
        {
          if (count != 4)
          {
            pw_aux[count] = key;
            count++;
            setDisplay(2);
          }
          else
          {
            count = 0;
            setDisplay(2);
          }
        }
      }

      keyboard_pressed_index = -1;
      keyboard_state = 0;
    }
    vTaskDelay(110 / portTICK_PERIOD_MS);
  }
}

//Clock of alert and presence
void Task_Clock(void *param)
{
  (void)param;

  while (1)
  {
    if (clk_presence == 10)
    {
      clk_presence++;
      digitalWrite(LED_PRESENCA_1, HIGH);
      digitalWrite(LED_PRESENCA_2, HIGH);
    }
    else if (clk_presence == 30 && alarm_mode != 3)
    {
      digitalWrite(LED_PRESENCA_1, LOW);
      digitalWrite(LED_PRESENCA_2, LOW);
    }
    else if (clk_presence <= 30)
    {
      clk_presence++;
    }

    //Alarm Mode
    if (alarm_mode == 2)
    {
      if (clk_alert <= 10)
        clk_alert++;
      else
      {
        alarm_mode = 3;
        digitalWrite(LED_ALARM_AMARELO, HIGH);
        Serial.println("Ligar para a policia . . .");
        setDisplay(112);
        clk_alert = 0;
      };
    }
    //Verify if client is exiting of house
    if(alarm_mode == 1 && exit_alarm == 1)
      if(clk_alert <= 10)  
        clk_alert++;
      else
      {
        exit_alarm = 0;  
        clk_alert = 0;
      }
      

    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

//Read and Write console
void Task_Terminal(void *param)
{
  (void)param;

  while (1)
  {
    String incoming = "";
    incoming = Serial.readString();
    if (incoming == "TEST")
    {
      digitalWrite(LED_ALARM_VERDE, 1);
      digitalWrite(LED_ALARM_AMARELO, 1);
      digitalWrite(LED_ALARM_VERMELHO, 1);
      digitalWrite(LED_ALERT_INCENDIO, 1);
      digitalWrite(LED_ALERT_INUNDACAO, 1);
      digitalWrite(LED_PRESENCA_1, 1);
      digitalWrite(LED_PRESENCA_2, 1);
      digitalWrite(BUZZ, 1);
      vTaskDelay(200 / portTICK_PERIOD_MS);
      digitalWrite(LED_ALARM_VERDE, 0);
      digitalWrite(LED_ALARM_AMARELO, 0);
      digitalWrite(LED_ALARM_VERMELHO, 0);
      digitalWrite(LED_ALERT_INCENDIO, 0);
      digitalWrite(LED_ALERT_INUNDACAO, 0);
      digitalWrite(LED_PRESENCA_1, 0);
      digitalWrite(LED_PRESENCA_2, 0);
      digitalWrite(BUZZ, 0);
    }
    vTaskDelay(200 / portTICK_PERIOD_MS);
  }
}

//Set display current status
void setDisplay(int mode)
{
  switch (mode)
  {
  case 1: // Initial set password
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("INSERT PASSWORD: ");
    break;
  case 2: // Add * to password
    lcd.setCursor(0, 1);
    lcd.print("    ");
    lcd.setCursor(0, 1);
    for (int i = 0; i < count; i++)
      lcd.print("*");
    break;
  case 3:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("SENSOR STATUS: ");
    lcd.setCursor(0, 1);
    lcd.print("M: ");
    lcd.print(alarm_mode == 3 ? "1 " : "0 ");
    lcd.print("|I: ");
    lcd.print(inundation ? "1 " : "0 ");
    lcd.print("|F: ");
    lcd.print(fire ? "1 " : "0 ");
    break;
   case 4:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("UNDEFINED!");
    break;
  case 112: // alert mode 3 (DANGER)
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("INTRUSAO!!!");
    lcd.setCursor(0, 0);
    break;
  default:
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ALARM STATUS:");
    lcd.setCursor(0, 1);
    if (alarm_mode == 0)
      lcd.print("DISARMED");
    else if (alarm_mode == 1)
      lcd.print("ARMED");
    else if (alarm_mode == 2)
      lcd.print("ALERT");
    else
      lcd.print("DANGER");
    break;
  }
}

//Enable/Disable alarm
void Enable_Disable_Alarm()
{
  if (alarm_mode >= 1)
    alarm_mode = 0;
  else
  {
    alarm_mode = 1; 
    exit_alarm = 1;
    clk_alert = 0;
  }
  pw_aux[0] = 'X';
  pw_aux[1] = 'X';
  pw_aux[2] = 'X';
  pw_aux[3] = 'X';
}

//verify password
bool checkPw()
{
  return (pw_aux[0] == pw[0] && pw_aux[1] == pw[1] && pw_aux[2] == pw[2] && pw_aux[3] == pw[3]) ? 1 : 0;
}

//get key pressed
char getKey(int index)
{
  switch (index)
  {
  case 0:
    return '1';
  case 1:
    return '2';
  case 2:
    return '3';
  case 3:
    return '4';
  case 4:
    return '5';
  case 5:
    return '6';
  case 6:
    return '7';
  case 7:
    return '8';
  case 8:
    return '9';
  case 9:
    return '0';
  case 10:
    return 'A';
  case 11:
    return 'B';
  case 12:
    return 'C';
  case 13:
    return 'D';
  case 14:
    return '*';
  case 15:
    return '#';
  }
  return 'N';
}
